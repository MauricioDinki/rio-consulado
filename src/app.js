// Import Styles
import './styl/main.styl'
// JS Files
import './js/carrousel'
import './js/effects'

// Import Images
require.context('./img', true, /\.(gif|png|jpe?g|svg|webp|ico)$/)
require.context('./video', true, /\.mp4$/)
