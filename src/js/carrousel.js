import { tns } from 'tiny-slider/src/tiny-slider'

// eslint-disable-next-line no-undef
if (document.querySelector('.home-banner-three-content')) {
  tns({
    container: '.home-banner-three-content',
    items: 1,
    controls: false,
    nav: true,
    navPosition: 'bottom',
    autoplay: false,
  })
}

// eslint-disable-next-line no-undef
if (document.querySelector('.about-video-list')) {
  let videosSlider = tns({
    container: '.about-video-list',
    items: 4,
    controls: false,
    nav: false,
    gutter: 15,
  })

  // eslint-disable-next-line no-undef
  document.getElementById('videoArrowLeft').onclick = function () {
    videosSlider.goTo('prev')
  }

  // eslint-disable-next-line no-undef
  document.getElementById('videoArrowRight').onclick = function () {
    videosSlider.goTo('next')
  }
}
