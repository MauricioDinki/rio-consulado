// eslint-disable-next-line no-undef
let coll = document.getElementsByClassName('collapsible')
for (let i = 0; i < coll.length; i++) {
  coll[i].addEventListener('click', function () {
    this.classList.toggle('active')
    let content = this.nextElementSibling
    if (content.style.display === 'block') {
      content.style.display = 'none'
    } else {
      content.style.display = 'block'
    }
  })
}
// eslint-disable-next-line no-undef
let collapsibleId = document.getElementsByClassName('collapsible-id')
for (let i = 0; i < collapsibleId.length; i++) {
  collapsibleId[i].addEventListener('click', function () {
    let contentId = collapsibleId[i].getAttribute('data-id')
    // eslint-disable-next-line no-undef
    let content = document.getElementById(contentId)
    if (content.style.display === 'flex') {
      content.style.display = 'none'
    } else {
      content.style.display = 'flex'
    }
  })
}
